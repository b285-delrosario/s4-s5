package com.zuitt;

import java.util.ArrayList;

public class Phonebook {
    private ArrayList<Contact> contacts;

    public Phonebook() {
        this.contacts = new ArrayList<Contact>();
    }

    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public ArrayList<Contact> getAContact() {
        return contacts;
    }

    public void setAContact(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public void addAContact(Contact contact) {
        this.contacts.add(contact);
    }
}

