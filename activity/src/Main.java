import com.zuitt.*;

public class Main {
    public static void main(String[] args) {

        Contact chanel = new Contact("Chanel","09123456789", "Parañaque City");
        Contact tommy = new Contact("Tommy","09987654321", "Las Piñas City");
        Contact roberta = new Contact("Roberta","09876345210", "Muntinlupa City");

        Phonebook myPhonebook = new Phonebook();
        myPhonebook.addAContact(chanel);
        myPhonebook.addAContact(tommy);
        myPhonebook.addAContact(roberta);

        for (Contact contact : myPhonebook.getAContact()) {
            System.out.println("==================================================");

            System.out.println(contact.getName().toUpperCase());

            System.out.println("--------------------------------------------------");

            System.out.println(contact.getName() + " has the following registered numbers:");
            System.out.println(contact.getContactNumber());

            System.out.println("--------------------------------------------------");

            System.out.println(contact.getName() + " has the following registered addresses:");
            System.out.println(contact.getAddress());
        }
    }
}
